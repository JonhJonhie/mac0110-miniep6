# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    c = [3]
     a = n
     h = 3 
     t = 0
    if n == 1
        return 1
    elseif n == 2
        return h
    else
        while n >1
            while a > 1
                h = h + 2
                a = a - 1
                push!(c, h)
                t = t + 1
            end
            a = n - 1
            n = n - 1
        end
        pop!(c)
        return c[t]
    end
end
# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    
    c = [3]
    d = m   
    m = m + 1
    a = m
    g = f = j = v = h = 3 
    p = t = 0
    x = m - 1
    w = v + (m - 1)
    
    if d == 1
        
        print("1 1 1")
        println("")
        
    else
        
        while m > 1
            
            while a > 1
                
                h = h + 2
                a = a - 1
                push!(c, h)
                t = t + 1
     
            end
            
            a = m - 1
            m = m - 1
            
        end
        
        pop!(c)
        
        if x == 2
            
            print(d )
            print(" ")
            print(d^3 )
            print( " ")
            print(3 )
            print(" ")
            print(5 )
            println(" ")
            
        elseif x == 3
            
            print(d)
            print(" ")
            print(d^3)
            print(" ")
            
            while v < w
                
                print(c[v])
                print(" ")
                v = v + 1
                
            end
            
            println()
       
        else 
            
            print(d )
            print(" ")
            print(d^3 )
            print(" ")
            
            while v < w
                
                print(c[(fiboalterado(d)+ p)] )
                print(" ")
                v = v + 1
                p = p + 1
                
            end
            
            println(" ")
            
        end  
    end
end

function fiboalterado(n)
    
    g = 3
    x = 3
    j = 3
    
    while x < n
        
        g = g + j
        j = j + 1
        x = x + 1
        
    end
    
    return g
    
end


function mostra_n(n)
    
    x = 1
    
    while x <= n
        
        imprime_impares_consecutivos(x)
        
        x = x + 1
        
    end
end
        

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()

